"""
derive some mocks from an input class
"""

from dataclasses import dataclass
from typing import Any


@dataclass
class FunctionCall:
    name: str
    args: tuple
    kwargs: dict
    response: Any


class Deride:
    class Func:
        def __init__(self, func, derided):
            self.func = func
            self.derided = derided

        def __call__(self, *args, **kwargs):
            try:
                resp = self.func(*args, **kwargs)
                self.derided.__derided_add_call((args, kwargs), self.func, resp)
                return resp
            except Exception as err:
                self.derided.__derided_add_call((args, kwargs), self.func, err)
                raise err

    def __init__(self, clazz):
        self.__derided_clazz = clazz
        self.__derided_calls = []

    def __getattr__(self, __name: str):
        """
        return the attribute requested from the underlying class
        """
        if __name.startswith("__derided"):
            __name = f"_Deride{__name}"
        if __name.startswith("_Deride") or __name.startswith("_Func"):
            # oh! this is for us! not the underlying class!
            __name = __name.replace("_Func", "_Deride")
            if __name == "_Deride__derided_add_call":
                return self.__derided_add_call
            else:
                return self.__dict__.get(__name)
        else:
            attr = getattr(self.__derided_clazz, __name)
            if attr is None:
                return attr
            if "__call__" in dir(attr):
                return Deride.Func(attr, self)
            else:
                pass

    def __derided_add_call(self, arguments, func, response):
        (args, kwargs) = arguments
        func_call = FunctionCall(func.__name__, args, kwargs, response)
        self.__derided_calls.append(func_call)


def get_calls(derided: Deride):
    return derided.__derided_calls

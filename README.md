# deride

derive a mock, kind of

## overview

Deride wraps a class or module and tracks functions called on that object along with the arguments to and response from that function.

Here's an example of wrapping the json module:
``` python
import json
from deride import Deride

derided_json = Deride(json)
obj = derided_json.loads('{"x":1}')
print(derided_json.dumps(obj, indent=2))
# {
#   "x": 1
# }
```

The motivating use case for this project is tracking calls to a third-party API in order to generate realistic test data for a mocked version of the API.

from deride import Deride, get_calls


class ExampleException(Exception):
    pass


class Example:
    def func(self, x, y):
        return x + y

    def raises(self):
        raise ExampleException()


def test_call_args_recorded():
    ex = Example()
    der = Deride(ex)
    der.func(1, 2)

    [call] = get_calls(der)
    assert call.args == (1, 2)
    assert call.kwargs == dict()
    assert call.name == "func"
    assert call.response == 1 + 2


def test_kwargs_recorded():
    ex = Example()
    der = Deride(ex)
    der.func(y=1, x=2)

    [call] = get_calls(der)
    assert call.args == ()
    assert call.kwargs == {"x": 2, "y": 1}
    assert call.name == "func"
    assert call.response == 1 + 2


def test_exceptions_recorded():
    example = Example()
    der = Deride(example)
    try:
        der.raises()
    except ExampleException:
        pass

    [call] = get_calls(der)
    assert call.args == ()
    assert call.kwargs == dict()
    assert call.name == "raises"
    assert isinstance(call.response, ExampleException)
